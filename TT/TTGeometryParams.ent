<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- TT Geometry parameters                -->
<!-- *  Authors: M.Needham, D.Volyanskyy * -->

<!-- an epsilon to avoid overlaps -->
<parameter name = "ttEps" value = "0.1*mm" />

<!-- z-Position of the TT station -->
<parameter name = "zPositionTT"    value  = "2485*mm" />

<!-- z-Offset of conical part of the beam pipe-->
<parameter name = "zOffsetCone"    value  = "15*mm" />

<!-- Aluminium frame -->
<parameter name = "WidthFrame"     value  = "2586*mm" /> <!-- CAD -->
<parameter name = "HeightFrame"    value  = "2628*mm" />
<parameter name = "ThicknessFrame" value = "15*mm" />
<parameter name = "WidthFrame1"    value = "1960*mm" />	 <!-- CAD -->
<parameter name = "HeightFrame1"   value = "2000*mm" />

<!-- TT isolation box -->
<parameter name = "BoxThickness"     value = "430.0*mm" />
<parameter name = "BoxWallThickness" value = "40.0*mm" />

<!-- distance between center of TTa and TTb -->
<parameter name = "SplitInZ" value = "270*mm "/>

<!-- Size of a TT half station -->
<parameter name = "StationThickness" value = "2*cutout+Jacket2Length" /> <!-- = 70.0mm -->
<parameter name = "HalfWidth"        value = "856*mm" /> <!-- CAD -->
<parameter name = "HalfHeight"       value = "822*mm" /> <!-- CAD -->

<!-- Cooling plates -->
<parameter name = "PlateTBThickness" value = "3.9*mm" /> <!-- calculated from weight: 6.13kg (Aluminium) -->
<parameter name = "PlateTBWidth" value = "346.0*mm" />
<parameter name = "PlateTBLength"  value = "1710.0*mm" /> <!-- CAD -->
<parameter name = "PlateLRThickness"  value = "6.0*mm" /> <!--  calculated from weight: 32kg (Copper) -->

<parameter name = "yPlateTB"      value = "HalfHeight+0.5*PlateTBThickness+ttEps" />

<!-- Jacket  -->
<parameter name = "JacketInnerRad"    value = "31.0*mm"/> <!-- CAD -->
<parameter name = "JacketInnerRadBig" value = "33.0*mm"/> <!-- CAD -->
<parameter name = "JacketOuterRad"    value = "60.0*mm"/> <!-- CAD -->
<parameter name = "cutout"            value = "25.0*mm"/> <!-- CAD -->
<parameter name = "Jacket1Length"     value = "5*mm"/>    <!-- CAD -->
<parameter name = "Jacket2Length"     value = "20*mm"/>   <!-- CAD -->

<!-- Layer layout-->
<parameter name = "DzLayer"    value = "36.5*mm" />
<parameter name = "DzLadderLR" value = "2.25*mm" />
<parameter name = "DzLadderTB" value = "9.75*mm" />
<parameter name = "zOffset"    value = "0.5*(cutout+Jacket2Length-DzLayer)" />
<parameter name = "Angle"    value = "5.0*degree"/>
<parameter name = "SinAngle" value = "sin(Angle)"/>
<parameter name = "CosAngle" value = "cos(Angle)"/>
<parameter name = "LayerThickness" value = "cutout-ttEps"/>

<!-- Overlap between ladders -->
<parameter name = "Overlap1" value = "3.5*mm" />
<parameter name = "Overlap2" value = "4.0*mm" />

<!-- Beam pipe hole -->
<parameter name = "HoleX" value = "37.0*mm+RailWidth-RailSlot"/> <!-- 38.7*mm -->
<parameter name = "HoleY" value = "37.0*mm"/>

<!-- TT sensors CMS-OB2 -->
<parameter name = "SensorHeight" value = "94.4*mm" /> <!-- CAD -->
<parameter name = "SensorWidth" value  = "96.4*mm" /> <!-- CAD -->
<parameter name = "SensorThickness" value = "0.5*mm" />

<!-- Gap between sensors for bonds within a half module -->
<parameter name = "bondGap" value = "0.2*mm" /> <!-- CAD -->
<!-- Gap between the innermost sensors of merged module -->
<parameter name = "halfModuleGap" value = "0.3*mm" /> <!-- measured: 3 times 0.3*mm; one time 0.2*mm -->

<!-- TT rail -->
<parameter name = "RailWidth"     value = "2.0*mm" />
<parameter name = "RailThickness" value = "5.5*mm" />
<parameter name = "RailHeight"    value = "7*SensorHeight + 6*bondGap" />
<parameter name = "RailSlot"      value = "0.3*mm" /> <!-- slot to merge sensor and rail -->

<!-- TT module -->
<parameter name = "moduleHeight" value = "7*SensorHeight + 6*bondGap + 0.5*halfModuleGap" /> <!-- 662.15*mm -->
<parameter name = "moduleWidth"  value = "SensorWidth + 2*(RailWidth-RailSlot)" /> <!-- 99.8*mm -->

<!-- Balcony -->
<parameter name = "WidthBalconyTTa"  value = "1369*mm"/> <!-- CAD -->
<parameter name = "WidthBalconyTTb"  value = "1554*mm"/> <!-- CAD -->
<parameter name = "HeightBalconyX"    value = "135.0*mm"/> <!-- CAD -->
<parameter name = "HeightBalconyU"    value = "138.0*mm"/>
<parameter name = "ThicknessBalcony" value = "8.35*mm"/> <!-- calculated from mass of all balconies in one quadrant: 4.5kg (Aluminium) -->
<parameter name = "yBalconyX"         value = " HalfHeight - 0.5*HeightBalconyX" />
<parameter name = "yBalconyU"         value = " HalfHeight - 0.5*HeightBalconyU" />

<!-- Steps between module positions -->
<parameter name="StepX1" value="SensorWidth-Overlap1"/>
<parameter name="StepX2" value="SensorWidth-Overlap2"/>
<parameter name="StepU1" value="(SensorWidth-Overlap1)/CosAngle"/>
<parameter name="StepU2" value="(SensorWidth-Overlap2)/CosAngle"/>

<!-- ============= -->
<!--   x layers    -->
<!-- ============= -->

<!-- X position of the Left&Right Modules for X-layers TTa&TTb  -->
<parameter name="XPosLR" value="HoleX+0.5*SensorWidth"/> <!-- 86.9*mm -->


<!-- Y position of the top/bottom Ladders for X layers TTa & TTb -->
<parameter name = "YPosTB_X" value = "HoleY+0.5*moduleHeight-0.5*halfModuleGap"/>

<!-- Mother volumes for left and right modules -->
<parameter name="xTTaXLR"     value = "XPosLR+1.5*(StepX1+StepX2)"/>
<parameter name="xTTbXLR"     value = "XPosLR+2*StepX1+1.5*StepX2"/>
<parameter name="xSizeTTaXLR" value = "3*(StepX1+StepX2)+moduleWidth"/> <!-- 679.3*mm -->
<parameter name="xSizeTTbXLR" value = "4*StepX1+3*StepX2+moduleWidth"/>

<!-- ============= -->
<!-- Stereo layers -->
<!-- ============= -->

<!-- Above/below beam pipe -->
<parameter name="XPosTB_UV" value = "YPosTB_X*SinAngle"/>
<parameter name="YPosTB_UV" value = "YPosTB_X*CosAngle"/>

<!-- y position of a Left/Right module -->
<parameter name="YPosLR_UV" value ="0.5*moduleHeight*CosAngle"/>

<!-- X position of the Left&Right Modules for U/V-layers TTa&TTb  -->
<parameter name="XPosLR_UV" value="(HoleX+0.5*SensorWidth)/CosAngle"/>

<!--  x position of the Left&Right Modules for stereo layers TTa and TTb -->
<parameter name="XPosLUT" value="XPosLR_UV-0.5*moduleHeight*SinAngle"/>
<parameter name="XPosLUB" value="XPosLR_UV+0.5*moduleHeight*SinAngle"/>
<parameter name="XPosRUT" value="XPosLR_UV+0.5*moduleHeight*SinAngle"/>
<parameter name="XPosRUB" value="XPosLR_UV-0.5*moduleHeight*SinAngle"/>
<parameter name="XPosLVT" value="XPosLR_UV+0.5*moduleHeight*SinAngle"/>
<parameter name="XPosLVB" value="XPosLR_UV-0.5*moduleHeight*SinAngle"/>
<parameter name="XPosRVT" value="XPosLR_UV-0.5*moduleHeight*SinAngle"/>
<parameter name="XPosRVB" value="XPosLR_UV+0.5*moduleHeight*SinAngle"/>

<!-- Mother volumes for left and right modules -->
<parameter name="xTTaULR" value="XPosLR_UV+1.5*(StepU1+StepU2)"/>
<parameter name="xSizeTTaULR" value="3*(StepU1+StepU2)+CosAngle*moduleWidth+2*SinAngle*moduleHeight"/>
<parameter name="xTTbVLR" value="XPosLR_UV+2*StepU1+1.5*StepU2"/>
<parameter name="xSizeTTbVLR" value="4*StepU1+3*StepU2+CosAngle*moduleWidth+SinAngle*2*moduleHeight"/>
