<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- ***************************************************************** -->
<!-- *  List of Outer Tracker geometry parameters.                   * -->
<!-- *                                                               * -->
<!-- *  Date: 01-12-2003                                             * -->
<!-- *  Authors: Jacopo Nardulli & Jeroen van Tilburg                * -->
<!-- ***************************************************************** -->

<!-- General parameters -->
<parameter name="Clearance" value="0.5*mm" />

<!-- Station parameters -->
<parameter name="StationXSize" value="8180.0*mm" />
<parameter name="StationYSize" value="6060.0*mm" />
<parameter name="StationZSize" value="220.0*mm" />
<parameter name="StationXCross1" value="465.0*mm" />
<parameter name="StationYCross1" value="293.0*mm" />
<parameter name="StationXCross2" value="1170.0*mm" />
<parameter name="StationYCross2" value="105.0*mm" />
<!-- <parameter name="T1Z" value="7948*mm"/>   -->
<!-- <parameter name="T2Z" value="8630*mm"/>   -->
<!-- <parameter name="T3Z" value="9315*mm"/>   -->
<!-- These are w.r.t. to the centre of the OT centre -->
<parameter name="T1Z" value="-683.5*mm" />
<parameter name="T2Z" value="-1.5*mm"   />
<parameter name="T3Z" value="+683.5*mm" />
<!-- station y-shift -->
<parameter name="yShiftCross" value="7.0*mm" />

<!-- Layer Parameters -->
<parameter name="LayerXSize"    value="6440.0*mm"   />
<parameter name="LayerYSize"    value="4835.0*mm"   />
<parameter name="X1StereoAngle" value="0.0*mrad"    />
<parameter name="X2StereoAngle" value="0.0*mrad"    />
<parameter name="UStereoAngle"  value="-5.0*degree" />
<parameter name="VStereoAngle"  value="5.0*degree"  />

<!-- Module Parameters -->
<parameter name="SideWallThick" value="0.6875*mm" />
<parameter name="SpcBtwnMdls" value="1.25*mm" />
<!-- Width of a module is about 340 mm -->
<!-- (64 + .5 straws) * 5.25mm(pitch in x) + 2*side walls -->
<parameter name="LModXSize" value="338.625*mm+2.*SideWallThick" />
<!-- This depends on the wire length in the top/bottom half -->
<!-- Need to check whether this OK with data -->
<!-- <parameter name="LModYSize" value="2395.0*mm + 36*mm + 2435*mm" /> -->
<parameter name="LModYSize" value="2383.0*mm + 36*mm + 2423*mm" />
<parameter name="LModXPitch" value="LModXSize+SpcBtwnMdls" />
<parameter name="S1ModXSize" value="LModXSize" />
<!-- <parameter name="S1ModYSize" value="2305.0*mm" /> -->
<!-- <parameter name="S1ModYSize" value="2320.5*mm + 12.5*mm" /> -->
<parameter name="S1ModYSize" value="2321.0*mm" />
<parameter name="S2ModXSize" value="LModXSize" />
<!-- <parameter name="S2ModYSize" value="2213.0*mm" /> -->
<!-- <parameter name="S2ModYSize" value="2221.0*mm + 12.5*mm" /> -->
<parameter name="S2ModYSize" value="2221.0*mm" />
<parameter name="S2ModXPitch" value="LModXPitch" />
<!-- Width of an S3 modules is about 170 mm -->
<!-- (32 + .5 straws) * 5.25mm(pitch in x) + 2*side walls -->
<parameter name="S3ModXSize" value="170.625*mm+2.*SideWallThick" />
<parameter name="S3ModYSize" value="S2ModYSize" />
<parameter name="S3ModXPitch" value="0.5*S3ModXSize+SpcBtwnMdls+0.5*LModXSize" />
<parameter name="EndModYSize" value="55.0*mm" />
<!-- LayerThick = ModuleThick which is about 32 mm -->
<parameter name="LayerThick" value="32.0*mm" />
<parameter name="ActiveThick" value="10.73*mm" />
<parameter name="SpcBtwnLrs" value="22.50*mm" />
<parameter name="SpcBwtnUV"  value="33.50*mm"  />
<parameter name="ModuleYCross1" value="384.0*mm" />
<parameter name="ModuleYCross2 " value="200.0*mm" />

<!-- Module parameter short cuts -->
<parameter name="LayerPitch" value="LayerThick+SpcBtwnLrs" />
<parameter name="UVPitch" value="LayerThick+SpcBwtnUV" />
<parameter name="PassiveThick" value="(LayerThick-ActiveThick)/2.0" />

<!-- CFrame Parameters -->
<parameter name="CFrameThick"           value="100.0*mm" />
<parameter name="CFramePlateThick"      value="10.0*mm" />
<parameter name="VerCFramePlateXSize"   value="800.0*mm" />
<parameter name="VerCFramePlateYSize"   value="6050.0*mm" />
<parameter name="ASideCFramePlateXSize" value="4170.0*mm" />
<parameter name="CSideCFramePlateXSize" value="4000.0*mm" />
<parameter name="HorCFramePlateYSize"   value="600.0*mm" />
<parameter name="SpcBtwVerCFramePlates" value="80.0*mm" />
<parameter name="VerCFramePlatePitch"   value="SpcBtwVerCFramePlates+CFramePlateThick" />
<parameter name="CFramePitch"           value="120.0*mm" />
<parameter name="CSideOffset"           value="-0.5*( LModXSize - S3ModXSize +  SpcBtwnMdls)" />
<parameter name="ASideOffset"           value="-0.5*( LModXSize - S3ModXSize -  SpcBtwnMdls)" />

<!-- Centres of C-Frames/rails and layers in LHCb frame. See survey EDMS note. -->
<!-- OTT1XU -->
<parameter name="T1XUY" value="28.4*mm*cos(beamAngle) -
                               7888.0*mm*sin(beamAngle)"              />
<parameter name="T1XUZ" value="28.4*mm*sin(beamAngle) +
                               7888.0*mm*cos(beamAngle) - 7948.0*mm"  />
<parameter name="T1X1Y" value="T1XUY + 0.5*LayerPitch*sin(beamAngle)" />
<parameter name="T1X1Z" value="T1XUZ - 0.5*LayerPitch*cos(beamAngle)" />
<parameter name="T1UY"  value="T1XUY - 0.5*LayerPitch*sin(beamAngle)" />
<parameter name="T1UZ"  value="T1XUZ + 0.5*LayerPitch*cos(beamAngle)" />

<!-- OTT1VX -->
<parameter name="T1VXY" value="28.8*mm*cos(beamAngle) -
                               8008.0*mm*sin(beamAngle)"              />
<parameter name="T1VXZ" value="28.8*mm*sin(beamAngle) +
                               8008.0*mm*cos(beamAngle) - 7948.0*mm"  />
<parameter name="T1VY"  value="T1VXY + 0.5*LayerPitch*sin(beamAngle)" />
<parameter name="T1VZ"  value="T1VXZ - 0.5*LayerPitch*cos(beamAngle)" />
<parameter name="T1X2Y" value="T1VXY - 0.5*LayerPitch*sin(beamAngle)" />
<parameter name="T1X2Z" value="T1VXZ + 0.5*LayerPitch*cos(beamAngle)" />

<!-- OTT2XU -->
<parameter name="T2XUY" value="30.9*mm*cos(beamAngle) -
                               8570.0*mm*sin(beamAngle)"              />
<parameter name="T2XUZ" value="30.9*mm*sin(beamAngle) +
                               8570.0*mm*cos(beamAngle) - 8630.0*mm"  />
<parameter name="T2X1Y" value="T2XUY + 0.5*LayerPitch*sin(beamAngle)" />
<parameter name="T2X1Z" value="T2XUZ - 0.5*LayerPitch*cos(beamAngle)" />
<parameter name="T2UY"  value="T2XUY - 0.5*LayerPitch*sin(beamAngle)" />
<parameter name="T2UZ"  value="T2XUZ + 0.5*LayerPitch*cos(beamAngle)" />

<!-- OTT2VX -->
<parameter name="T2VXY" value="31.3*mm*cos(beamAngle) -
                               8690.0*mm*sin(beamAngle)"              />
<parameter name="T2VXZ" value="31.3*mm*sin(beamAngle) +
                               8690.0*mm*cos(beamAngle) - 8630.0*mm"  />
<parameter name="T2VY"  value="T2VXY + 0.5*LayerPitch*sin(beamAngle)" />
<parameter name="T2VZ"  value="T2VXZ - 0.5*LayerPitch*cos(beamAngle)" />
<parameter name="T2X2Y" value="T2VXY - 0.5*LayerPitch*sin(beamAngle)" />
<parameter name="T2X2Z" value="T2VXZ + 0.5*LayerPitch*cos(beamAngle)" />

<!-- OTT3XU -->
<parameter name="T3XUY" value="33.3*mm*cos(beamAngle) -
                               9255.0*mm*sin(beamAngle)"              />
<parameter name="T3XUZ" value="33.3*mm*sin(beamAngle) +
                               9255.0*mm*cos(beamAngle) - 9315.0*mm"  />
<parameter name="T3X1Y" value="T3XUY + 0.5*LayerPitch*sin(beamAngle)" />
<parameter name="T3X1Z" value="T3XUZ - 0.5*LayerPitch*cos(beamAngle)" />
<parameter name="T3UY"  value="T3XUY - 0.5*LayerPitch*sin(beamAngle)" />
<parameter name="T3UZ"  value="T3XUZ + 0.5*LayerPitch*cos(beamAngle)" />

<!-- OTT3VX -->
<parameter name="T3VXY" value="33.8*mm*cos(beamAngle) -
                               9375.0*mm*sin(beamAngle)"              />
<parameter name="T3VXZ" value="33.8*mm*sin(beamAngle) +
                               9375.0*mm*cos(beamAngle) - 9315.0*mm"  />
<parameter name="T3VY"  value="T3VXY + 0.5*LayerPitch*sin(beamAngle)" />
<parameter name="T3VZ"  value="T3VXZ - 0.5*LayerPitch*cos(beamAngle)" />
<parameter name="T3X2Y" value="T3VXY - 0.5*LayerPitch*sin(beamAngle)" />
<parameter name="T3X2Z" value="T3VXZ + 0.5*LayerPitch*cos(beamAngle)" />
