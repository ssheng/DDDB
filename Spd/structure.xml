<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE DDDB SYSTEM "git:/DTD/structure.dtd"[
<!ENTITY GeomParameters SYSTEM "git:/Spd/GeomParam.xml">
<!ENTITY DetElemParameters SYSTEM "git:/Spd/DetElemParam.xml">
]>
<DDDB>
<!--
  **************************************************************************
  *                                                                        *
  *  Version 2.0 of the first detailed Spd description by Grigori Rybkine  *
  *                         Grigori.Rybkine@cern.ch                        *
  *                                                                        *
  **************************************************************************
-->
  &GeomParameters;

  <!-- ########### Spd effective cell sizes ########### -->
  <parameter name="InCell"      value="SpdModXYSize/12" />
  <parameter name="MidCell"     value="SpdModXYSize/8"  />
  <parameter name="OutCell"     value="SpdModXYSize/4"  />
  <parameter name="OutID"       value="0"  />
  <parameter name="MidID"       value="1"  />
  <parameter name="InID"        value="2"  />
  <parameter name="ASide"       value="1"  />
  <parameter name="CSide"       value="-1"  />

  <!-- ***************************************************************** -->
  <!--                  Define detector elements for Spd                 -->
  <!-- ***************************************************************** -->

  <detelem name = "Spd" classID = "8900">
    <author> Grigori Rybkine Grigori.Rybkine@cern.ch </author>
    <version>             3.0                        </version>
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Spd/Installation/Spd"
                  condition = "/dd/Conditions/Alignment/Spd/SpdSystem"
                  support = "/dd/Structure/LHCb/DownstreamRegion"
                  npath   = "SpdSubsystem"/>
    <detelemref href = "#SpdA"   />
    <detelemref href = "#SpdC"   />

    <conditioninfo name      = "Hardware"
                   condition = "/dd/Conditions/ReadoutConf/Spd/Hardware" />

    <!--- WARNING : this is NOT a bug : spd has no own readout but uses Prs one -->
    <conditioninfo name      = "Readout"
                   condition = "/dd/Conditions/ReadoutConf/Prs/Readout" />

    <conditioninfo name      = "Gain"
                   condition = "/dd/Conditions/Calibration/Spd/Gain" />

    <conditioninfo name      = "Reco"
                   condition = "/dd/Conditions/Calibration/Spd/Reco" />

    <conditioninfo name      = "Calibration"
                   condition = "/dd/Conditions/Calibration/Spd/Calibration" />

    <conditioninfo name      = "Quality"
                   condition = "/dd/Conditions/Calibration/Spd/Quality" />

    &DetElemParameters;

  </detelem>

  <!-- ***************************************************************** -->

  <detelem name = "SpdA"    classID="8901" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Spd/Installation/SpdLeft"
                  condition = "/dd/Conditions/Alignment/Spd/SpdASystem"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Spd"
                  npath   = "SpdA"/>

    <userParameter name="CaloSide" type="int"> ASide </userParameter>

    <detelemref href = "#SpdAOuter"   />
    <detelemref href = "#SpdAMiddle"   />
    <detelemref href = "#SpdAInner"   />

  </detelem>

  <detelem name = "SpdC"    classID="8901" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Spd/Installation/SpdRight"
                  condition = "/dd/Conditions/Alignment/Spd/SpdCSystem"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Spd"
                  npath   = "SpdC"/>

    <userParameter name="CaloSide" type="int"> CSide </userParameter>

    <detelemref href = "#SpdCOuter"   />
    <detelemref href = "#SpdCMiddle"   />
    <detelemref href = "#SpdCInner"   />

  </detelem>

  <!-- ***************************************************************** -->

  <detelem name = "SpdAInner"    classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Spd/Installation/InnerSectionLeft"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Spd/SpdA"
                  npath   = "SpdAInner"/>

    <userParameter name="CellSize" type="double"> InCell </userParameter>
    <userParameter name="Area"     type="int"> InID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*SpdInnXSize</userParameter>
    <userParameter name="YSize" type="double"> SpdInnYSize </userParameter>

  </detelem>

  <detelem name = "SpdAMiddle"  classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Spd/Installation/MiddleSectionLeft"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Spd/SpdA"
                  npath   = "SpdAMiddle"/>

    <userParameter name="CellSize" type="double"> MidCell </userParameter>
    <userParameter name="Area"     type="int"> MidID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*SpdMidXSize</userParameter>
    <userParameter name="YSize" type="double"> SpdMidYSize </userParameter>


  </detelem>

  <detelem name = "SpdAOuter"   classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Spd/Installation/OuterSectionLeft"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Spd/SpdA"
                  npath   = "SpdAOuter"/>

    <userParameter name="CellSize" type="double"> OutCell </userParameter>
    <userParameter name="Area"     type="int"> OutID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*SpdOutXSize</userParameter>
    <userParameter name="YSize" type="double"> SpdOutYSize </userParameter>

  </detelem>


  <detelem name = "SpdCInner"    classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Spd/Installation/InnerSectionRight"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Spd/SpdC"
                  npath   = "SpdCInner"/>

    <userParameter name="CellSize" type="double"> InCell </userParameter>
    <userParameter name="Area"     type="int"> InID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*SpdInnXSize</userParameter>
    <userParameter name="YSize" type="double"> SpdInnYSize </userParameter>

  </detelem>

  <detelem name = "SpdCMiddle"  classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Spd/Installation/MiddleSectionRight"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Spd/SpdC"
                  npath   = "SpdCMiddle"/>

    <userParameter name="CellSize" type="double"> MidCell </userParameter>
    <userParameter name="Area"     type="int"> MidID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*SpdMidXSize</userParameter>
    <userParameter name="YSize" type="double"> SpdMidYSize </userParameter>

  </detelem>

  <detelem name = "SpdCOuter"   classID="8902" >
    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Spd/Installation/OuterSectionRight"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Spd/SpdC"
                  npath   = "SpdCOuter"/>

    <userParameter name="CellSize" type="double"> OutCell </userParameter>
    <userParameter name="Area"     type="int"> OutID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*SpdOutXSize</userParameter>
    <userParameter name="YSize" type="double"> SpdOutYSize </userParameter>

  </detelem>


</DDDB>
