<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE DDDB SYSTEM "git:/DTD/geometry.dtd">

<DDDB>

<!-- ************************************************************** -->
<!-- * BEAM PIPE                                                  * -->
<!-- * Sections in AfterMagnetRegion                              * -->
<!-- *   from z = 7620mm to z = 11900mm                           * -->
<!-- *                                                            * -->
<!-- * UX85-3 in AfterMagnet After T                              * -->
<!-- *   from z = 9437mm to z = 11900mm                           * -->
<!-- *                                                            * -->
<!-- * Author: Gloria Corti                                       * -->
<!-- *                                                            * -->
<!-- * Consists of:                                               * -->
<!-- *     - conical sections of 10 mrad, 1.9mm, 2.0mm, 2.1mm,    * -->
<!-- *       2.3mm and 2.4mm thick                                * -->
<!-- *     - external cylindrical section for support             * -->
<!-- *   and corresponding vacuum conical sections                * -->
<!-- ************************************************************** -->

 <catalog name = "PipeAfterT">
   <logvolref href="#lvUX853AfterT"/>
   <logvolref href="#lvUX853Cone07B"/>
   <logvolref href="#lvUX853Cone08"/>
   <logvolref href="#lvUX853Cone09"/>
   <logvolref href="#lvUX853Cone10"/>
   <logvolref href="#lvUX853Cone11"/>
   <logvolref href="#lvUX853Cone12A"/>
   <logvolref href="#lvUX853Vacuum07B"/>
 </catalog>

<!-- Whole section: combination of sections -->
 <logvol name="lvUX853AfterT">

<!-- Pipe of Be -->
   <physvol name   = "pvUX853Cone07B"
            logvol = "/dd/Geometry/AfterMagnetRegion/PipeAfterT/lvUX853Cone07B">
     <posXYZ z = "UX853Cone07BZpos"/>
   </physvol>

   <physvol name   = "pvUX853Cone08"
            logvol = "/dd/Geometry/AfterMagnetRegion/PipeAfterT/lvUX853Cone08">
     <posXYZ z = "UX853Cone08Zpos"/>
   </physvol>

   <physvol name   = "pvUX853Cone09"
            logvol = "/dd/Geometry/AfterMagnetRegion/PipeAfterT/lvUX853Cone09">
     <posXYZ z = "UX853Cone09Zpos"/>
   </physvol>

   <physvol name   = "pvUX853Cone10"
            logvol = "/dd/Geometry/AfterMagnetRegion/PipeAfterT/lvUX853Cone10">
     <posXYZ z = "UX853Cone10Zpos"/>
   </physvol>

   <physvol name   = "pvUX853Cone11"
            logvol = "/dd/Geometry/AfterMagnetRegion/PipeAfterT/lvUX853Cone11">
     <posXYZ z = "UX853Cone11Zpos"/>
   </physvol>

   <physvol name   = "pvUX853Cone12A"
            logvol = "/dd/Geometry/AfterMagnetRegion/PipeAfterT/lvUX853Cone12A">
     <posXYZ z = "UX853Cone12AZpos"/>
   </physvol>

<!-- Now the vacuum inside -->
   <physvol name   = "pvUX853Vacuum07B"
            logvol = "/dd/Geometry/AfterMagnetRegion/PipeAfterT/lvUX853Vacuum07B">
     <posXYZ z = "0.5*UX853AfterTLenght"/>
   </physvol>

 </logvol>

<!-- UX85-3 Cone 10 mrad of Beryllium 1.9 mm thick - Split T/Rich2 -->
 <logvol name = "lvUX853Cone07B" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-3-Cone10mrad-07B"
         sizeZ         = "UX853Cone07BLenght"
         innerRadiusPZ = "UX853Cone07BRadiusZmax"
         innerRadiusMZ = "UX853Cone07BRadiusZmin"
         outerRadiusPZ = "UX853Cone07BRadiusZmax + UX853Cone07BThick"
         outerRadiusMZ = "UX853Cone07BRadiusZmin + UX853Cone07BThick"/>
 </logvol>

<!-- UX85-3 Cone 10 mrad of Beryllium 2.0 mm thick -->
 <logvol name = "lvUX853Cone08" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-3-Cone10mrad-08"
         sizeZ         = "UX853Cone08Lenght"
         innerRadiusPZ = "UX853Cone08RadiusZmax"
         innerRadiusMZ = "UX853Cone08RadiusZmin"
         outerRadiusPZ = "UX853Cone08RadiusZmax + UX853Cone08Thick"
         outerRadiusMZ = "UX853Cone08RadiusZmin + UX853Cone08Thick"/>
 </logvol>

<!-- UX85-3 Cone 10 mrad of Beryllium 2.1 mm thick -->
 <logvol name = "lvUX853Cone09" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-3-Cone10mrad-09"
         sizeZ         = "UX853Cone09Lenght"
         innerRadiusPZ = "UX853Cone09RadiusZmax"
         innerRadiusMZ = "UX853Cone09RadiusZmin"
         outerRadiusPZ = "UX853Cone09RadiusZmax + UX853Cone09Thick"
         outerRadiusMZ = "UX853Cone09RadiusZmin + UX853Cone09Thick"/>
 </logvol>

<!-- UX85-3 Cone 10 mrad of Beryllium 2.3 mm thick -->
 <logvol name = "lvUX853Cone10" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-3-Cone10mrad-10"
         sizeZ         = "UX853Cone10Lenght"
         innerRadiusPZ = "UX853Cone10RadiusZmax"
         innerRadiusMZ = "UX853Cone10RadiusZmin"
         outerRadiusPZ = "UX853Cone10RadiusZmax + UX853Cone10Thick"
         outerRadiusMZ = "UX853Cone10RadiusZmin + UX853Cone10Thick"/>
 </logvol>

<!-- UX85-3 Cone 10 mrad of Beryllium 2.4 mm thick -->
 <logvol name = "lvUX853Cone11" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-3-Cone10mrad-11"
         sizeZ         = "UX853Cone11Lenght"
         innerRadiusPZ = "UX853Cone11RadiusZmax"
         innerRadiusMZ = "UX853Cone11RadiusZmin"
         outerRadiusPZ = "UX853Cone11RadiusZmax + UX853Cone11Thick"
         outerRadiusMZ = "UX853Cone11RadiusZmin + UX853Cone11Thick"/>
 </logvol>

<!-- UX85-3 Cone 10 mrad of Beryllium cylindrical part for support -->
<!-- Split Rich2/Downstream                                        -->
 <logvol name = "lvUX853Cone12A" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-3-Cone10mrad-12A"
         sizeZ         = "UX853Cone12ALenght"
         innerRadiusPZ = "UX853Cone12ARadiusZmax"
         innerRadiusMZ = "UX853Cone12ARadiusZmin"
         outerRadiusPZ = "UX853Cone12AOuterRadius"
         outerRadiusMZ = "UX853Cone12AOuterRadius"/>
 </logvol>

<!-- Vacuum in UX85-3 in whole AfterT lenght -->
<logvol name = "lvUX853Vacuum07B" material = "Vacuum">
   <cons name          = "UX85-3-Vacuum-07B"
         sizeZ         = "UX853AfterTLenght"
         outerRadiusMZ = "UX853Cone07BRadiusZmin"
         outerRadiusPZ = "UX853Cone12ARadiusZmax"/>
 </logvol>

</DDDB>
