<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE DDDB SYSTEM "git:/DTD/geometry.dtd">
<DDDB>

<!-- ************************************************************** -->
<!-- * BEAM PIPE                                                  * -->
<!-- * Sections in MagnetRegion                                   * -->
<!-- *   from z = 2270 mm to z = 7620 mm                          * -->
<!-- *                                                            * -->
<!-- * Author: Gloria Corti                                       * -->
<!-- *                                                            * -->
<!-- * Consists of                                                * -->
<!-- *   UX85-2:                                                  * -->
<!-- *     - conical sections of 10 mrad of different thickness   * -->
<!-- *     - two flanges                                          * -->
<!-- *   and corresponding vaccum conical sections                * -->
<!-- ************************************************************** -->

<!-- UX85-2 Pipes of Al and Be -->
 <catalog name = "UX852">
   <logvolref href="#lvUX852"/>
   <logvolref href="#lvUX852Flange01"/>
   <logvolref href="#lvUX852Cone02"/>
   <logvolref href="#lvUX852Cone03"/>
   <logvolref href="#lvUX852Cone04"/>
   <logvolref href="#lvUX852Cone04A"/>
   <logvolref href="#lvUX852Cone04B"/>
   <logvolref href="#lvUX852Cone04C"/>
   <logvolref href="#lvUX852Cone04Rib"/>
   <logvolref href="#lvUX852Cone05"/>
   <logvolref href="#lvUX852Cone06"/>
   <logvolref href="#lvUX852Cone07"/>
   <logvolref href="#lvUX852Cone08"/>
   <logvolref href="#lvUX852Flange09"/>
   <logvolref href="#lvUX852Vacuum01"/>
 </catalog>

<!-- UX85-2 Pipes of Al and Be -->
 <logvol name="lvUX852">

     <physvol name   = "pvUX852Flange01"
              logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX852/lvUX852Flange01">
       <posXYZ z = "UX852Flange01Zpos"/>
     </physvol>

     <physvol name   = "pvUX852Cone02"
              logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX852/lvUX852Cone02">
       <posXYZ z = "UX852Cone02Zpos"/>
     </physvol>

     <physvol name   = "pvUX852Cone03"
              logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX852/lvUX852Cone03">
       <posXYZ z = "UX852Cone03Zpos"/>
     </physvol>

     <physvol name   = "pvUX852Cone04"
              logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX852/lvUX852Cone04">
       <posXYZ z = "UX852Cone04Zpos"/>
     </physvol>

     <physvol name   = "pvUX852Cone05"
              logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX852/lvUX852Cone05">
       <posXYZ z = "UX852Cone05Zpos"/>
     </physvol>

     <physvol name   = "pvUX852Cone06"
              logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX852/lvUX852Cone06">
       <posXYZ z = "UX852Cone06Zpos"/>
     </physvol>

     <physvol name   = "pvUX852Cone07"
              logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX852/lvUX852Cone07">
       <posXYZ z = "UX852Cone07Zpos"/>
     </physvol>

     <physvol name   = "pvUX852Cone08"
              logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX852/lvUX852Cone08">
       <posXYZ z = "UX852Cone08Zpos"/>
     </physvol>

     <physvol name   = "pvUX852Flange09"
              logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX852/lvUX852Flange09">
       <posXYZ z = "UX852Flange09Zpos"/>
     </physvol>

      <physvol name   = "pvUX852Vacuum01"
               logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX852/lvUX852Vacuum01">
       <posXYZ z = "0.5*UX852Vacuum01Lenght"/>
     </physvol>

 </logvol>

<!-- UX85-2 Flange  -->
 <logvol name = "lvUX852Flange01" material = "Pipe/PipeAl2219F">
   <cons name          = "UX85-2-Flange01"
         sizeZ         = "UX852Flange01Lenght"
         innerRadiusMZ = "UX852Flange01RadiusZmin"
         innerRadiusPZ = "UX852Flange01RadiusZmax"
         outerRadiusMZ = "UX852Flange01OuterRadius"
         outerRadiusPZ = "UX852Flange01OuterRadius"/>
 </logvol>

<!-- UX85-2 Cone 10 mrad of Alumimium 1.5mm thick for flange  -->
 <logvol name = "lvUX852Cone02" material = "Pipe/PipeAl2219F">
   <cons name          = "UX85-2-Cone10mrad-02"
         sizeZ         = "UX852Cone02Lenght"
         innerRadiusMZ = "UX852Cone02RadiusZmin"
         innerRadiusPZ = "UX852Cone02RadiusZmax"
         outerRadiusMZ = "UX852Cone02RadiusZmin + UX852Cone02Thick"
         outerRadiusPZ = "UX852Cone02RadiusZmax + UX852Cone02Thick"/>
 </logvol>

<!-- UX85-2 Cone 10 mrad of Beryllium 1.0mm thick -->
 <logvol name = "lvUX852Cone03" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-2-Cone10mrad-03"
         sizeZ         = "UX852Cone03Lenght"
         innerRadiusMZ = "UX852Cone03RadiusZmin"
         innerRadiusPZ = "UX852Cone03RadiusZmax"
         outerRadiusMZ = "UX852Cone03RadiusZmin + UX852Cone03Thick"
         outerRadiusPZ = "UX852Cone03RadiusZmax + UX852Cone03Thick"/>
 </logvol>

<!-- UX85-2 Cone 10 mrad of Beryllium section for support A+B+C+flange -->
 <logvol name = "lvUX852Cone04" >
   <physvol name   = "pvUX852Cone04A"
            logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX852/lvUX852Cone04A">
     <posXYZ z = "-0.5*UX852Cone04Lenght + 0.5*UX852Cone04ALenght"/>
   </physvol>
   <physvol name   = "pvUX852Cone04B"
            logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX852/lvUX852Cone04B">
     <posXYZ z = "-0.5*UX852Cone04Lenght + UX852Cone04ALenght + 0.5*UX852Cone04BLenght"/>
   </physvol>
   <physvol name   = "pvUX852Cone04C"
            logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX852/lvUX852Cone04C">
     <posXYZ z = "-0.5*UX852Cone04Lenght + UX852Cone04ALenght + UX852Cone04BLenght + 0.5*UX852Cone04CLenght"/>
   </physvol>
   <physvol name   = "pvUX852Cone04Rib"
            logvol = "/dd/Geometry/MagnetRegion/PipeInMagnet/UX852/lvUX852Cone04Rib">
     <posXYZ z = "-0.5*UX852Cone04Lenght + UX852Cone04ALenght + 0.5*UX852Cone04BLenght"/>
   </physvol>
 </logvol>

<!-- UX85-2 Cone 10 mrad of Beryllium section for support A  -->
 <logvol name = "lvUX852Cone04A" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-2-Cone10mrad-04A"
         sizeZ         = "UX852Cone04ALenght"
         innerRadiusMZ = "UX852Cone04ARadiusZmin"
         innerRadiusPZ = "UX852Cone04ARadiusZmax"
         outerRadiusMZ = "UX852Cone04ARadiusZmin + UX852Cone04AThick"
         outerRadiusPZ = "UX852Cone04ARadiusZmax + UX852Cone04AThick"/>
 </logvol>

<!-- UX85-2 Cone 10 mrad of Beryllium section for support B - cylindrical on outside -->
 <logvol name = "lvUX852Cone04B" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-2-Cone10mrad-04B"
         sizeZ         = "UX852Cone04BLenght"
         innerRadiusMZ = "UX852Cone04BRadiusZmin"
         innerRadiusPZ = "UX852Cone04BRadiusZmax"
         outerRadiusMZ = "UX852Cone04BOuterRadius"
         outerRadiusPZ = "UX852Cone04BOuterRadius"/>
 </logvol>

<!-- UX85-2 Cone 10 mrad of Beryllium section for support C -->
 <logvol name = "lvUX852Cone04C" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-2-Cone10mrad-04C"
         sizeZ         = "UX852Cone04CLenght"
         innerRadiusMZ = "UX852Cone04CRadiusZmin"
         innerRadiusPZ = "UX852Cone04CRadiusZmax"
         outerRadiusMZ = "UX852Cone04CRadiusZmin + UX852Cone04CThick"
         outerRadiusPZ = "UX852Cone04CRadiusZmax + UX852Cone04CThick"/>
 </logvol>

<!-- UX85-2 Cone 10 mrad of Beryllium section for support - flange surrounds B section -->
 <logvol name = "lvUX852Cone04Rib" material = "Pipe/PipeBeTV56">
   <tubs name        = "UX85-2-Cone04-Rib"
         sizeZ       = "UX852Cone04RibLenght"
         innerRadius = "UX852Cone04RibInnerRadius"
         outerRadius = "UX852Cone04RibOuterRadius"/>
 </logvol>

<!-- UX85-2 Cone 10 mrad of Beryllium 1.0 mm thick -->
 <logvol name = "lvUX852Cone05" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-2-Cone10mrad-05"
         sizeZ         = "UX852Cone05Lenght"
         innerRadiusMZ = "UX852Cone05RadiusZmin"
         innerRadiusPZ = "UX852Cone05RadiusZmax"
         outerRadiusMZ = "UX852Cone05RadiusZmin + UX852Cone05Thick"
         outerRadiusPZ = "UX852Cone05RadiusZmax + UX852Cone05Thick"/>
 </logvol>

<!-- UX85-2 Cone 10 mrad of Beryllium 1.1 mm thick -->
 <logvol name = "lvUX852Cone06" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-2-Cone10mrad-06"
         sizeZ         = "UX852Cone06Lenght"
         innerRadiusMZ = "UX852Cone06RadiusZmin"
         innerRadiusPZ = "UX852Cone06RadiusZmax"
         outerRadiusMZ = "UX852Cone06RadiusZmin + UX852Cone06Thick"
         outerRadiusPZ = "UX852Cone06RadiusZmax + UX852Cone06Thick"/>
 </logvol>

<!-- UX85-2 Cone 10 mrad of Beryllium 1.3 mm thick -->
 <logvol name = "lvUX852Cone07" material = "Pipe/PipeBeTV56">
   <cons name          = "UX85-2-Cone10mrad-07"
         sizeZ         = "UX852Cone07Lenght"
         innerRadiusMZ = "UX852Cone07RadiusZmin"
         innerRadiusPZ = "UX852Cone07RadiusZmax"
         outerRadiusMZ = "UX852Cone07RadiusZmin + UX852Cone07Thick"
         outerRadiusPZ = "UX852Cone07RadiusZmax + UX852Cone07Thick"/>
 </logvol>

<!-- UX85-2 Cone 10 mrad of Aluminum 2.0 mm thick -->
 <logvol name = "lvUX852Cone08" material = "Pipe/PipeAl2219F">
   <cons name          = "UX85-2-Cone10mrad-08"
         sizeZ         = "UX852Cone08Lenght"
         innerRadiusMZ = "UX852Cone08RadiusZmin"
         innerRadiusPZ = "UX852Cone08RadiusZmax"
         outerRadiusMZ = "UX852Cone08RadiusZmin + UX852Cone08Thick"
         outerRadiusPZ = "UX852Cone08RadiusZmax + UX852Cone08Thick"/>
 </logvol>

<!-- UX85-2 Flange -->
 <logvol name = "lvUX852Flange09" material = "Pipe/PipeAl2219F">
   <cons name          = "UX85-2-Flange09"
         sizeZ         = "UX852Flange09Lenght"
         innerRadiusMZ = "UX852Flange09RadiusZmin"
         innerRadiusPZ = "UX852Flange09RadiusZmax"
         outerRadiusMZ = "UX852Flange09OuterRadius"
         outerRadiusPZ = "UX852Flange09OuterRadius"/>
 </logvol>


<!-- Vacuum in UX852 -->
 <logvol name = "lvUX852Vacuum01" material = "Vacuum">
   <cons name          = "UX85-2-Vacuum-01"
         sizeZ         = "UX852Vacuum01Lenght"
         outerRadiusMZ = "UX852Flange01RadiusZmin"
         outerRadiusPZ = "UX852Flange09RadiusZmax"/>
 </logvol>

</DDDB>
